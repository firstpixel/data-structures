package textgen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Random;

/** 
 * An implementation of the MTG interface that uses a list of lists.
 * @author UC San Diego Intermediate Programming MOOC team 
 */
public class MarkovTextGeneratorLoL implements MarkovTextGenerator {

	// The list of words with their next words
	private List<ListNode> wordList; 
	
	// The starting "word"
	private String starter;
	
	// The random number generator
	private Random rnGenerator;
	
	public MarkovTextGeneratorLoL(Random generator)
	{
		wordList = new LinkedList<ListNode>();
		starter = "";
		rnGenerator = generator;
	}
	
	
	/** Train the generator by adding the sourceText */
	@Override
	public void train(String sourceText)
	{
		// TODO: Implement this method
		/*
		 * 
		  	set "starter" to be the first word in the text  
			set "prevWord" to be starter
			for each word "w" in the source text starting at the second word
			   check to see if "prevWord" is already a node in the list
			      if "prevWord" is a node in the list
			         add "w" as a nextWord to the "prevWord" node
			      else
			         add a node to the list with "prevWord" as the node's word
			         add "w" as a nextWord to the "prevWord" node
			    set "prevWord" = "w"
			
			add starter to be a next word for the last word in the source text.
		 * */
		String [] wordArray = sourceText.split(" ");
		starter = wordArray[0];
		//String[] wordFromSecondArray = Arrays.copyOfRange(wordArray, 1, wordArray.length);
		String prevWord = starter;
		
		Map<String,Integer> hasWord = new HashMap<String,Integer>();
		String w = null;
		wordList = new ArrayList<ListNode>();
		ListNode wordNode = new ListNode(prevWord);
		for(int i=1; i<wordArray.length; i++){
			w = wordArray[i];
			//wordList.add(wordNode);
			if( isWord(w)){
				if(hasWord.containsKey(prevWord)){
					for(ListNode wn:wordList){
						if(wn.getWord().equals(prevWord)){
							wn.addNextWord(w);
						}
					}
					hasWord.put(prevWord, hasWord.get(prevWord)+1);
				}else{
					wordNode = new ListNode(prevWord);
					wordList.add(wordNode);
					wordNode.addNextWord(w);
					hasWord.put(prevWord, 1);
				}
				prevWord = w;
			}
			
		}	
		
		if(hasWord.containsKey(w)){
			for(ListNode wn:wordList){
				if(wn.getWord().equals(w)){
					wn.addNextWord(starter);
				}
			}
		}else{
			wordNode = new ListNode(w);
			wordNode.addNextWord(starter);
			wordList.add(wordNode);
			
		}

		//System.out.println(wordList.toString());
	}
	
	private boolean isWord(String tok)
	{
	    // Note: This is a fast way of checking whether a string is a word
	    // You probably don't want to change it.
		return !(tok.indexOf("!") >=0 || tok.indexOf(".") >=0 || tok.indexOf("?")>=0);
	}
	
	/** 
	 * Generate the number of words requested.
	 */
	@Override
	public String generateText(int numWords) {
	    // TODO: Implement this method
		/*
	 	set "currWord" to be the starter word
		set "output" to be ""
		add "currWord" to output
		while you need more words
		   find the "node" corresponding to "currWord" in the list
		   select a random word "w" from the "wordList" for "node"
		   add "w" to the "output"
		   set "currWord" to be "w" 
		   increment number of words added to the list
	*/
		String currWord = wordList.get(0).getWord();
		String output = "";
		output += currWord;
		int i = 1;
		while(i<numWords){
			for(ListNode wn:wordList){
				if(wn.getWord().equals(currWord)){
					String w = wn.getRandomNextWord(rnGenerator);
					output += " " + w ;
					currWord = w;
					break;
				}
			}
			i++;
		}
		
		
		return output;
	}
	
	
	// Can be helpful for debugging
	@Override
	public String toString()
	{
		String toReturn = "";
		for (ListNode n : wordList)
		{
			toReturn += n.toString();
		}
		return toReturn;
	}
	
	/** Retrain the generator from scratch on the source text */
	@Override
	public void retrain(String sourceText)
	{
		// TODO: Implement this method.
		wordList = new LinkedList<ListNode>();
		starter = "";
		train(sourceText);
	}
	
	// TODO: Add any private helper methods you need here.
	
	
	/**
	 * This is a minimal set of tests.  Note that it can be difficult
	 * to test methods/classes with randomized behavior.   
	 * @param args
	 */
	public static void main(String[] args)
	{
		// feed the generator a fixed random value for repeatable behavior
		MarkovTextGeneratorLoL gen = new MarkovTextGeneratorLoL(new Random(42));
		String textString1 = "hi there hi Leo";
		//System.out.println(textString1);
		gen.train(textString1);
		//System.out.println(gen);
		System.out.println(gen.generateText(4));
		
		String textString = "Hello.  Hello there.  This is a test.  Hello there.  Hello Bob.  Test again.";
		System.out.println(textString);
		gen.train(textString);
		System.out.println(gen);
		System.out.println(gen.generateText(20));
		String textString2 = "You say yes, I say no, "+
				"You say stop, and I say go, go, go, "+
				"Oh no. You say goodbye and I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello. "+
				"I say high, you say low, "+
				"You say why, and I say I don't know. "+
				"Oh no. "+
				"You say goodbye and I say hello, hello, hello. "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello. "+
				"Why, why, why, why, why, why, "+
				"Do you say goodbye. "+
				"Oh no. "+
				"You say goodbye and I say hello, hello, hello. "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello. "+
				"You say yes, I say no, "+
				"You say stop and I say go, go, go. "+
				"Oh, oh no. "+
				"You say goodbye and I say hello, hello, hello. "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello, hello, hello,";
		System.out.println(textString2);
		gen.train(textString2);
		System.out.println(gen);
		System.out.println(gen.generateText(20));
	}

}

/** Links a word to the next words in the list 
 * You should use this class in your implementation. */
class ListNode
{
    // The word that is linking to the next words
	private String word;
	
	// The next words that could follow it
	private List<String> nextWords;
	
	ListNode(String word)
	{
		this.word = word;
		nextWords = new LinkedList<String>();
	}
	
	public String getWord()
	{
		return word;
	}

	public void addNextWord(String nextWord)
	{
		nextWords.add(nextWord);
	}
	
	public String getRandomNextWord(Random generator)
	{
		// TODO: Implement this method
	    // The random number generator should be passed from 
	    // the MarkovTextGeneratorLoL class
		
		int rnd = generator.nextInt(nextWords.size());
	    return nextWords.get(rnd);
	}

	public String toString()
	{
		String toReturn = word + ": ";
		for (String s : nextWords) {
			toReturn += s + "->";
		}
		toReturn += "\n";
		return toReturn;
	}
	
	
	
}


