package textgen;

import java.util.AbstractList;


/** A class that implements a doubly linked list
 * 
 * @author UC San Diego Intermediate Programming MOOC team
 *
 * @param <E> The type of the elements stored in the list
 */
public class MyLinkedList<E> extends AbstractList<E> {
	LLNode<E> head;
	LLNode<E> tail;
	int size;

	/** Create a new empty LinkedList */
	public MyLinkedList() {
		// TODO: Implement this method
		size = 0;
		head = new LLNode<E>(null);
		tail = new LLNode<E>(null);
		head.next = tail;
		tail.prev = head;
	}

	/**
	 * Appends an element to the end of the list
	 * @param element The element to add
	 */
	public boolean add(E element ) 
	{
		// TODO: Implement this method
		if(element == null){
			throw new NullPointerException();
			
		}else {
			LLNode<E> node = new LLNode<E>(element);
			LLNode<E> currentNode = tail;
			currentNode.next = node;
			node.prev = currentNode;
			node.next = null;
			tail = node;
			size++;
			if(size==1){
				head = node;
			}
			return true;
		}
	}

	/** Get the element at position index 
	 * @throws IndexOutOfBoundsException if the index is out of bounds. */
	public E get(int index) throws IndexOutOfBoundsException
	{
		if(index >= size || index<0){
			throw new IndexOutOfBoundsException();
		}else{
			// TODO: Implement this method.
			int i=0;
			LLNode<E> node = head;
			while(i!=index && i < size){
				i++;
				node = node.next;
			}
			return node.data;
		}
	}

	/**
	 * Add an element to the list at the specified index
	 * @param The index where the element should be added
	 * @param element The element to add
	 */
	public void add(int index, E element ) 
	{
		// TODO: Implement this method
		if(element == null){
			throw new NullPointerException();
		}else
		if(index > size  || index<0){
			throw new IndexOutOfBoundsException();
		}else{
			// TODO: Implement this method.
			int i=0;
			LLNode<E> nodeNext = head;
			LLNode<E> nodePrev = head;
			while(i!=index && i < size){
				nodePrev = nodeNext;
				nodeNext = nodeNext.next;
				i++;
			}
			LLNode<E> newNode= new LLNode<E>(element);
			
			if(i > 0){
				newNode.prev = nodePrev;
				nodePrev.next = newNode;
			}else{
				newNode.prev = null;
				
			}
			newNode.next = nodeNext;
			nodeNext.prev = newNode;
	
			if(index==size-1){
				tail = newNode;
			}
			if(i == 0){
				head = newNode;
			}
	
			size++;
		}
	}

	/** Return the size of the list */
	public int size() 
	{
		// TODO: Implement this method
		return size;
	}

	/** Remove a node at the specified index and return its data element.
	 * @param index The index of the element to remove
	 * @return The data element removed
	 * @throws IndexOutOfBoundsException If index is outside the bounds of the list
	 * 
	 */
	public E remove(int index) 
	{
		// TODO: Implement this method
		if(index >= size || index<0){
			throw new IndexOutOfBoundsException();
		}else{
			int i=0;
			LLNode<E> node = head;
			while(i!=index && i < size){
				node = node.next;
				i++;
			}
			LLNode<E> prevNode = node.prev; 
			LLNode<E> nextNode = node.next;
			
			if(prevNode != null) prevNode.next = nextNode;
			if(nextNode != null) nextNode.prev = prevNode;
			E element = node.data;
			node = null;
			if(i==0){
				head = nextNode;
			}
			if(i==size-1){
				tail = prevNode;
			}
			size--;
			return element;
		}
	}

	/**
	 * Set an index position in the list to a new element
	 * @param index The index of the element to change
	 * @param element The new element
	 * @return The element that was replaced
	 * @throws IndexOutOfBoundsException if the index is out of bounds.
	 */
	public E set(int index, E element) 
	{
		// TODO: Implement this method
		if(element == null){
			throw new NullPointerException();
		}else
		if(index > size || index<0){
			throw new IndexOutOfBoundsException();
		}else{
			int i=0;
			LLNode<E> node = head;
			while(i!=index && i < size){
				node = node.next;
				i++;
			}
			node.data = element;
			if(index==0){
				head = node;
			}
			if(index==size-1){
				tail = node;
			}
			
			return node.data;
		}
	}   
	public E getHead(){
		LLNode<E> llNode = (LLNode<E>)this.get(0);
		return llNode.data;
	}
	public E getTail(){
		LLNode<E> llNode = (LLNode<E>)this.get(this.size-1);
		return llNode.data;
	}
}

class LLNode<E> 
{
	LLNode<E> prev;
	LLNode<E> next;
	E data;

	// TODO: Add any other methods you think are useful here
	// E.g. you might want to add another constructor

	public LLNode(E e) 
	{
		this.data = e;
		this.prev = null;
		this.next = null;
	}
	
}
