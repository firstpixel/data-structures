/**
 * 
 */
package textgen;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

/**
 * @author UC San Diego MOOC team
 *
 */
public class MyLinkedListTester {

	private static final int LONG_LIST_LENGTH = 10; 

	MyLinkedList<String> shortList;
	MyLinkedList<Integer> emptyList;
	MyLinkedList<Integer> longerList;
	MyLinkedList<Integer> list1;
	
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		// Feel free to use these lists, or add your own
	    shortList = new MyLinkedList<String>();
		shortList.add("A");
		shortList.add("B");
		emptyList = new MyLinkedList<Integer>();
		longerList = new MyLinkedList<Integer>();
		for (int i = 0; i < LONG_LIST_LENGTH; i++)
		{
			longerList.add(i);
		}
		list1 = new MyLinkedList<Integer>();
		list1.add(65);
		list1.add(21);
		list1.add(42);
		
	}

	
	/** Test if the get method is working correctly.
	 */
	/*You should not need to add much to this method.
	 * We provide it as an example of a thorough test. */
	@Test
	public void testGet()
	{
		//test empty list, get should throw an exception
		try {
			emptyList.get(0);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
			
		}
		
		// test short list, first contents, then out of bounds
		assertEquals("Check first", "A", shortList.get(0));
		assertEquals("Check second", "B", shortList.get(1));
		
		try {
			shortList.get(-1);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		
		}
		try {
			shortList.get(2);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		
		}
		// test longer list contents
		for(int i = 0; i<LONG_LIST_LENGTH; i++ ) {
			assertEquals("Check "+i+ " element", (Integer)i, longerList.get(i));
		}
		
		// test off the end of the longer array
		try {
			longerList.get(-1);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		
		}
		try {
			longerList.get(LONG_LIST_LENGTH);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		}
		
	}
	
	
	/** Test removing an element from the list.
	 * We've included the example from the concept challenge.
	 * You will want to add more tests.  */
	@Test
	public void testRemove()
	{
		int a = list1.remove(0);

		try {
			list1.remove(5);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		
		}
		try {
			list1.remove(-1);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		
		}
		
		assertEquals("Remove: check a is correct ", 65, a);
		assertEquals("Remove: check element 0 is correct ", (Integer)21, list1.get(0));
		assertEquals("Remove: check size is correct ", 2, list1.size());
		
		assertEquals("Remove: check head element is correct ", (Integer)21, list1.head.data);
		
		assertEquals("Remove: check tail element is correct ", (Integer)42, list1.tail.data);

	}
	
	/** Test adding an element into the end of the list, specifically
	 *  public boolean add(E element)
	 * */
	@Test
	public void testAddEnd()
	{
		
		try {
			list1.add(null);
			fail("Check out of bounds");
		}
		catch (NullPointerException e) {
		
		}
       
		list1.add(30);
		list1.set(0,21);
		assertEquals("testAddEnd: check element 0 is correct ", (Integer)21, list1.get(0));
		assertEquals("testAddEnd: check size is correct ", 4, list1.size());
		assertEquals("testAddEnd: check tail element is correct ", (Integer)30, list1.tail.data);
		assertEquals("testAddEnd: check last element is correct ", (Integer)30, list1.get(list1.size()-1));
		
	}

	
	/** Test the size of the list */
	@Test
	public void testSize()
	{
		
		
		
		list1.set(0,65);
		list1.set(1,21);
		list1.set(2,42);

		int a = list1.remove(0);
		list1.add(0,65);
		list1.set(1,33);
		
		assertEquals("testSize: check element 0 is correct ", (Integer)65, list1.get(0));
		assertEquals("testAddEnd: check element 1 is correct ", (Integer)33, list1.get(1));
		assertEquals("testAddEnd: check size is correct ", 3, list1.size());
		assertEquals("testAddEnd: check tail element is correct ", (Integer)42, list1.tail.data);
		list1.set(1,21);
	}

	
	
	/** Test adding an element into the list at a specified index,
	 * specifically:
	 * public void add(int index, E element)
	 * */
	@Test
	public void testAddAtIndex()
	{
		
		try {
			list1.add(-1,10);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		
		}
		
		try {
			list1.add(1,null);
			fail("Check out of bounds");
		}
		catch (NullPointerException e) {
		
		}
        
		list1.add(1,65);
		int a = list1.remove(1);
		list1.add(1,33);
		list1.set(2,30);
		assertEquals("testAddAtIndex: check a is correct ", 65, a);
		assertEquals("testAddAtIndex: check element 0 is correct ", (Integer)65, list1.get(0));
		assertEquals("testAddAtIndex: check element 1 is correct ", (Integer)33, list1.get(1));
		assertEquals("testAddAtIndex: check element 2 is correct ", (Integer)30, list1.get(2));
		assertEquals("testAddAtIndex: check size is correct ", 4, list1.size());
		assertEquals("testAddAtIndex: check tail element is correct ", (Integer)42, list1.tail.data);
		
	}
	
	/** Test setting an element in the list */
	@Test
	public void testSet()
	{
		try {
			list1.set(-1,10);
			fail("Check out of bounds");
		}
		catch (IndexOutOfBoundsException e) {
		
		}
		
		try {
			list1.set(1,null);
			fail("Check out of bounds");
		}
		catch (NullPointerException e) {
		
		}
		
		list1.set(0,66);
		assertEquals("testSet: check element 0 is correct ", (Integer)66, list1.get(0));
		assertEquals("testSet: check element 1 is correct ", (Integer)21, list1.get(1));
		assertEquals("testSet: check size is correct ", 3, list1.size());
		assertEquals("testSet: check tail element is correct ", (Integer)42, list1.tail.data);
		assertEquals("testSet: check head element is correct ", (Integer)66, list1.head.data);

		
	}
	
	
	
	/** Test setting a new list */
	@Test
	public void testNewList()
	{
	   
		MyLinkedList<Integer> newList = new MyLinkedList<Integer>();
		newList.add(65);
		newList.remove(0);
		newList.add(65);
		
		
		assertEquals("testSet: check element 0 is correct ", (Integer)65, newList.get(0));
		assertEquals("testSet: check size is correct ", 1, newList.size());
		assertEquals("testSet: check tail element is correct ", (Integer)65, newList.tail.data);
		assertEquals("testSet: check head element is correct ", (Integer)65, newList.head.data);
		
	}
	
	
}
